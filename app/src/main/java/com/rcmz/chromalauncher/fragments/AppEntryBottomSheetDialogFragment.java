package com.rcmz.chromalauncher.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.rcmz.chromalauncher.singletons.ChromaApplication;
import com.rcmz.chromalauncher.model.AppEntry;
import com.rcmz.chromalauncher.R;
import com.rcmz.chromalauncher.model.Utils;
import com.rcmz.chromalauncher.activities.SettingsActivity;

public class AppEntryBottomSheetDialogFragment extends BottomSheetDialogFragment {
    private AppEntry appEntry;
    private @ColorInt int savedNavigationBarColor;

    public AppEntryBottomSheetDialogFragment() {
    }

    public AppEntryBottomSheetDialogFragment(AppEntry appEntry) {
        this.appEntry = appEntry;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        // disable collapsed state
        getDialog().setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                View sheet = ((BottomSheetDialog) dialog).findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior<View> behavior = BottomSheetBehavior.from(sheet);
                behavior.setSkipCollapsed(true);
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        // set navigation bar color
        new Handler(Looper.myLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                savedNavigationBarColor = getActivity().getWindow().getNavigationBarColor();
                getActivity().getWindow().setNavigationBarColor(appEntry.color);
            }
        }, 200);

        // fill menu
        View sheet = inflater.inflate(R.layout.app_entry_bottom_sheet_dialog, container, false);
        @ColorInt int backgroundColor = appEntry.color;
        @ColorInt int foregroundColor = Utils.deltaE(appEntry.color, Color.BLACK) > Utils.deltaE(appEntry.color, Color.WHITE) ? Color.BLACK : Color.WHITE;
        sheet.setBackgroundColor(backgroundColor);

        // app label
        TextView appLabelTextView = sheet.findViewById(R.id.bottom_sheet_dialog_app_label);
        appLabelTextView.setTextColor(foregroundColor);
        appLabelTextView.setText(appEntry.label);
        Drawable appEntryIcon = appEntry.icon.getConstantState().newDrawable();
        Drawable defaultDrawable = appLabelTextView.getCompoundDrawablesRelative()[0];
        appEntryIcon.setBounds(defaultDrawable.getBounds());
        appLabelTextView.setCompoundDrawablesRelative(appEntryIcon, null, null, null);

        // information
        TextView informationTextView = sheet.findViewById(R.id.bottom_sheet_dialog_information);
        informationTextView.setTextColor(foregroundColor);
        informationTextView.getCompoundDrawablesRelative()[0].setTint(foregroundColor);
        informationTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.setData(Uri.parse("package:" + appEntry.packageName));
                startActivity(intent, ChromaApplication.slideUpBundle);
                dismiss();
            }
        });

        // hide
        TextView hideTextView = sheet.findViewById(R.id.bottom_sheet_dialog_hide);
        hideTextView.setTextColor(foregroundColor);
        hideTextView.getCompoundDrawablesRelative()[0].setTint(foregroundColor);
        hideTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appEntry.hidden = true;
                ChromaApplication.getAppRepository().insert(appEntry);
                dismiss();
            }
        });

        // uninstall
        TextView uninstallTextView = sheet.findViewById(R.id.bottom_sheet_dialog_uninstall);
        uninstallTextView.setTextColor(foregroundColor);
        uninstallTextView.getCompoundDrawablesRelative()[0].setTint(foregroundColor);
        uninstallTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DELETE);
                intent.setData(Uri.parse("package:" + appEntry.packageName));
                startActivity(intent);
                dismiss();
            }
        });

        // launcher settings
        TextView launcherSettingsTextView = sheet.findViewById(R.id.bottom_sheet_dialog_launcher_settings);
        launcherSettingsTextView.setTextColor(foregroundColor);
        launcherSettingsTextView.getCompoundDrawablesRelative()[0].setTint(foregroundColor);
        launcherSettingsTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChromaApplication.getContext(), SettingsActivity.class);
                startActivity(intent, ChromaApplication.slideUpBundle);
                dismiss();
            }
        });

        return sheet;
    }

    @Override
    public void onDestroyView() {
        getActivity().getWindow().setNavigationBarColor(savedNavigationBarColor);
        super.onDestroyView();
    }
}
