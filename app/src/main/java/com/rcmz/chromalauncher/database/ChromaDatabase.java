package com.rcmz.chromalauncher.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.rcmz.chromalauncher.model.AppEntry;

@Database(entities = {AppEntry.class}, version = 4)
public abstract class ChromaDatabase extends RoomDatabase {
    public abstract AppsDao appsDao();
}
