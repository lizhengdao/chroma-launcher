package com.rcmz.chromalauncher.model;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;

import androidx.annotation.ColorInt;
import androidx.core.graphics.ColorUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BetterPalette {
    private static class Swatch {
        public @ColorInt
        final int color;
        public final int population;

        public Swatch(@ColorInt int color, int population) {
            this.color = color;
            this.population = population;
        }
    }

    private static class Box extends HashMap<double[], Integer> {
        private final int dimension;

        public Box(int dimension) {
            this.dimension = dimension;
        }

        public int getDimension() {
            return dimension;
        }

        public int getPopulation() {
            int population = 0;

            for (double[] key : keySet()) {
                population += get(key);
            }

            return population;
        }

        public double getAverage(int axis) {
            double total = 0f;

            for (double[] key : keySet()) {
                total += key[axis] * get(key);
            }

            return total / getPopulation();
        }

        public double[] getAverage() {
            double[] average = new double[getDimension()];

            for (int i = 0; i < getDimension(); i++) {
                average[i] = getAverage(i);
            }

            return average;
        }

        public double getError(int axis) {
            double average = getAverage(axis);
            double error = 0f;

            for (double[] key : keySet()) {
                double e = key[axis] - average;
                error += e * e * get(key);
            }

            return error;
        }

        public double getRelativeError(int axis) {
            return getError(axis) / getPopulation();
        }

        public Box[] split(final int axis) {
            List<double[]> list = new ArrayList<>(keySet());

            Collections.sort(list, new Comparator<double[]>() {
                @Override
                public int compare(double[] k1, double[] k2) {
                    for (int d = 0; d < getDimension(); d++) {
                        int a = (axis + d) % getDimension();
                        int axisCompare = Double.compare(k1[a], k2[a]);
                        if (axisCompare != 0) return axisCompare;
                    }

                    Log.e("COLOR ARE EQUALS", "this shouldn't be possible");
                    return 0;
                }
            });

            Box subBox1 = new Box(getDimension());
            List<double[]> subList1 = list.subList(0, list.size()/2);
            for (double[] key : subList1) subBox1.put(key, get(key));

            Box subBox2 = new Box(getDimension());
            List<double[]> subList2 = list.subList(list.size()/2, list.size());
            for (double[] key : subList2) subBox2.put(key, get(key));

            return new Box[] {subBox1, subBox2};
        }
    }

    private final List<Swatch> swatches;

    public BetterPalette(Drawable drawable) {
        swatches = new ArrayList<>();

        // sharpening prevent the mysterious icons change from influencing the palette generation
        Bitmap bitmap = Utils.generateBitmap(drawable, 108, 108, 4);
        int[] pixels = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(pixels, 0 , bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());

        // create a histogram of all the pixels
        Map<Integer, Integer> IntPixelMap = new HashMap<>();

        for (@ColorInt int pixel : pixels) {
            if (Color.alpha(pixel) == 255) { // remove transparent pixels
                IntPixelMap.put(pixel, (IntPixelMap.containsKey(pixel) ? IntPixelMap.get(pixel) : 0) + 1);
            }
        }

        // convert those pixels into the desired color format
        Map<double[], Integer> LabPixelMap = new HashMap<>();

        for (@ColorInt int pixel : IntPixelMap.keySet()) {
            double[] lab = { 0, 0, 0 };
            ColorUtils.colorToLAB(pixel, lab);
            if (Utils.saturation(pixel) * Utils.value(pixel) > 0.25f) { // remove gray pixels
                LabPixelMap.put(lab, IntPixelMap.get(pixel));
            }
        }

        if (LabPixelMap.isEmpty()) {
            swatches.add(new Swatch(Color.BLACK, 1));
            return;
        }

        // use median cut to extract primary colors
        Box inputBox = new Box(3);
        inputBox.putAll(LabPixelMap);
        List<Box> boxes = medianCut(inputBox, new int[] { 1, 2 });

        // convert boxes to swatches
        for (Box box : boxes) {
            double[] lab = box.getAverage();
            @ColorInt int color = ColorUtils.LABToColor(lab[0], lab[1], lab[2]);
            swatches.add(new Swatch(color, box.getPopulation()));
        }

        // sort swatches by population
        Collections.sort(swatches, new Comparator<Swatch>() {
            @Override
            public int compare(Swatch s1, Swatch s2) {
                if (s1.population == s2.population) return Integer.compare(s1.color, s2.color);
                return -Integer.compare(s1.population, s2.population);
            }
        });
    }

    private List<Box> medianCut(Box inputBox, int[] axes) {
        List<Box> boxes = new ArrayList<>();
        boxes.add(inputBox);

        while (boxes.size() < 4) {
            Box bestBox = null;
            int bestAxis = -1;
            double maxError = -1f;

            for (Box box : boxes) {
                for (int axis : axes) {
                    double error = box.getError(axis);

                    if (error > maxError) {
                        maxError = error;
                        bestBox = box;
                        bestAxis = axis;
                    }
                }
            }

//            if (bestBox.getRelativeError(bestAxis) < 400) {
//                break;
//            }

            Box[] newBoxes = bestBox.split(bestAxis);
            boxes.remove(bestBox);
            boxes.add(newBoxes[0]);
            boxes.add(newBoxes[1]);
        }

        return boxes;
    }

    public List<Integer> getColors() {
        List<Integer> colors = new ArrayList<>();

        for (Swatch swatch : swatches) {
            colors.add(swatch.color);
        }

        return colors;
    }

    public int getPopulation(@ColorInt int color) {
        for (Swatch swatch : swatches) {
            if (swatch.color == color) {
                return swatch.population;
            }
        }

        return 0;
    }

    @ColorInt
    public int getMainColor() {
        return swatches.size() > 0 ? swatches.get(0).color : Color.BLACK;
    }
}
