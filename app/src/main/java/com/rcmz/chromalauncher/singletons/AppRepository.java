package com.rcmz.chromalauncher.singletons;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.room.Room;

import com.rcmz.chromalauncher.activities.LauncherActivity;
import com.rcmz.chromalauncher.database.ChromaDatabase;
import com.rcmz.chromalauncher.model.AppEntry;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

public class AppRepository {
    private ChromaDatabase db;

    public AppRepository() {
        db = Room.databaseBuilder(ChromaApplication.getContext(), ChromaDatabase.class, "chroma-database")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();

        refresh();
    }

    public LiveData<List<AppEntry>> getVisible() {
        return db.appsDao().getVisible();
    }

    public LiveData<List<AppEntry>> getHidden() {
        return db.appsDao().getHidden();
    }

    public void insert(AppEntry appEntry) {
        db.appsDao().insert(appEntry);
    }

    public void refresh() {
        Log.d("WARNING", "refreshing apps");

        List<AppEntry> newAppEntries = new ArrayList<>();

        // query for all installed apps on the device
        Intent queryIntent = new Intent(Intent.ACTION_MAIN);
        queryIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> resolveInfos = ChromaApplication.getContext().getPackageManager().queryIntentActivities(queryIntent, 0);

        for (ResolveInfo resolveInfo : resolveInfos) {
            String className = resolveInfo.activityInfo.name;

            // don't create an app entry for the launcher activity
            if (className.equals(LauncherActivity.class.getName())) {
                continue;
            }

            // conserve the hidden state
            AppEntry oldAppEntry = db.appsDao().get(className);
            boolean hidden = oldAppEntry != null && oldAppEntry.hidden;
            AppEntry newAppEntry = new AppEntry(resolveInfo.activityInfo, hidden);
            newAppEntries.add(newAppEntry);
        }

        db.appsDao().deleteAll();
        db.appsDao().insertAll(newAppEntries);
    }
}
