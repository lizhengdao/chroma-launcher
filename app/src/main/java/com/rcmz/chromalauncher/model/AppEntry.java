package com.rcmz.chromalauncher.model;

import android.content.ComponentName;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.palette.graphics.Palette;
import androidx.room.Entity;
import androidx.room.Ignore;

import com.rcmz.chromalauncher.singletons.ChromaApplication;

@Entity(tableName = "AppEntries", primaryKeys = { "packageName", "className" })
public class AppEntry {
    @NonNull
    public String packageName;

    @NonNull
    public String className;

    @NonNull
    public String label;

    public int iconRes;

    @Ignore
    public Drawable icon;

    @Ignore
    public BetterPalette palette;

    @ColorInt
    public int color;

    public boolean hidden;

    public AppEntry(ActivityInfo activityInfo, boolean hidden) {
        PackageManager pm = ChromaApplication.getContext().getPackageManager();

        packageName = activityInfo.packageName;
        className = activityInfo.name;
        label = activityInfo.loadLabel(pm).toString();
        icon = activityInfo.loadIcon(pm);
        iconRes = activityInfo.icon;

        palette = new BetterPalette(icon);
        color = palette.getMainColor();

//        Bitmap bitmap = Bitmap.createBitmap(108, 108, Bitmap.Config.ARGB_8888);
//        Palette palette = Palette.from(bitmap).generate();
//        color = palette.getVibrantColor(Color.BLACK);

        this.hidden = hidden;
    }

    public AppEntry(@NonNull String packageName, @NonNull String className, @NonNull String label, int iconRes, @ColorInt int color, boolean hidden) {
        PackageManager pm = ChromaApplication.getContext().getPackageManager();

        this.packageName = packageName;
        this.className = className;
        this.label = label;
        this.iconRes = iconRes;
        this.color = color;
        this.hidden = hidden;

        try {
            icon = pm.getActivityIcon(new ComponentName(packageName, className));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
