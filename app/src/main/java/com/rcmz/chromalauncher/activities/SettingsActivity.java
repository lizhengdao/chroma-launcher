package com.rcmz.chromalauncher.activities;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SeekBarPreference;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.rcmz.chromalauncher.model.AppEntry;
import com.rcmz.chromalauncher.R;
import com.rcmz.chromalauncher.adapters.HiddenAppAdapter;
import com.rcmz.chromalauncher.singletons.ChromaApplication;

import java.util.List;

public class SettingsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        ViewPager2 viewPager = findViewById(R.id.settings_activity_view_pager);

        viewPager.setAdapter(new FragmentStateAdapter(this) {
            @Override
            public int getItemCount() {
                return 2;
            }

            @NonNull
            @Override
            public Fragment createFragment(int position) {
                switch (position) {
                    case 0: return new SettingsPreferenceFragment();
                    case 1: return new HiddenAppsPreferenceFragment();
                }

                return null;
            }
        });

        new TabLayoutMediator((TabLayout) findViewById(R.id.settings_activity_tab_layout), viewPager, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                switch (position) {
                    case 0: tab.setText(R.string.settings_tab_settings); break;
                    case 1: tab.setText(R.string.settings_tab_hidden_apps); break;
                }
            }
        }).attach();
    }

    public static class SettingsPreferenceFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.app_sorting_preferences, rootKey);

            findPreference("restart").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    System.exit(0);
                    return true;
                }
            });

            listDependency("wallpaper_opacity", "background_mode", "wallpaper");

            valueAsSummary("wallpaper_opacity", "%");
            valueAsSummary("icon_size", "%");
            valueAsSummary("number_of_columns", "");
            valueAsSummary("hue_offset", "°");
        }

        private void valueAsSummary(String key, final String suffix) {
            final SeekBarPreference seekBarPref = findPreference(key);
            seekBarPref.setSummary(seekBarPref.getValue() + suffix);
            seekBarPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference p, Object newValue) {
                    seekBarPref.setSummary(((int) newValue) + suffix);
                    return true;
                }
            });
        }

        private void listDependency(String prefKey, String listPrefKey, final String value) {
            final Preference preference = findPreference(prefKey);
            final ListPreference listPreference = findPreference(listPrefKey);
            preference.setEnabled(listPreference.getValue().equals(value));
            listPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference p, Object newValue) {
                    preference.setEnabled(((String) newValue).equals(value));
                    return true;
                }
            });
        }
    }

    public static class HiddenAppsPreferenceFragment extends Fragment {
        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            return inflater.inflate(R.layout.hidden_apps_settings_fragment, container, false);
        }

        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            final ListView listView = view.findViewById(R.id.hidden_apps_list_view);

            ChromaApplication.getAppRepository().getHidden().observe(getViewLifecycleOwner(), new Observer<List<AppEntry>>() {
                @Override
                public void onChanged(List<AppEntry> appEntries) {
                    listView.setAdapter(new HiddenAppAdapter(appEntries));

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            AppEntry appEntry = (AppEntry) listView.getAdapter().getItem(position);
                            appEntry.hidden = false;
                            ChromaApplication.getAppRepository().insert(appEntry);
                        }
                    });
                }
            });
        }
    }
}
