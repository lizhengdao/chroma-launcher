package com.rcmz.chromalauncher.singletons;

import android.app.ActivityOptions;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import androidx.room.Room;

import com.rcmz.chromalauncher.R;
import com.rcmz.chromalauncher.database.ChromaDatabase;

public class ChromaApplication extends Application {
    public static Bundle slideUpBundle;
    private static ChromaApplication instance;
    private static AppRepository appRepository;

    public void onCreate() {
        super.onCreate();
        slideUpBundle = ActivityOptions.makeCustomAnimation(this, R.anim.slide_up, R.anim.do_nothing).toBundle();
        instance = this;
        appRepository = new AppRepository();
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    public static AppRepository getAppRepository() {
        return appRepository;
    }
}
