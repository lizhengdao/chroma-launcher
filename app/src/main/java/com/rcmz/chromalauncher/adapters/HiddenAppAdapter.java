package com.rcmz.chromalauncher.adapters;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.rcmz.chromalauncher.singletons.ChromaApplication;
import com.rcmz.chromalauncher.model.AppEntry;
import com.rcmz.chromalauncher.R;

import java.util.List;

public class HiddenAppAdapter extends BaseAdapter {
    private List<AppEntry> appEntries;

    public HiddenAppAdapter(List<AppEntry> appEntries) {
        this.appEntries = appEntries;
    }

    @Override
    public int getCount() {
        return appEntries.size();
    }

    @Override
    public AppEntry getItem(int position) {
        return appEntries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AppEntry appEntry = getItem(position);
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        TextView itemTextView = (TextView) inflater.inflate(R.layout.hidden_app_list_item, parent, false);

        itemTextView.setText(appEntry.label);

        Drawable[] defaultDrawables = itemTextView.getCompoundDrawablesRelative();
        Drawable appEntryIcon = appEntry.icon.getConstantState().newDrawable();
        appEntryIcon.setBounds(defaultDrawables[0].getBounds());
        itemTextView.setCompoundDrawablesRelative(appEntryIcon, defaultDrawables[1], defaultDrawables[2], defaultDrawables[3]);

        return itemTextView;
    }
}
