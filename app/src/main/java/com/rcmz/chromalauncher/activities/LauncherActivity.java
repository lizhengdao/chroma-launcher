package com.rcmz.chromalauncher.activities;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import androidx.annotation.ColorInt;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.preference.PreferenceManager;

import com.rcmz.chromalauncher.model.AppEntry;
import com.rcmz.chromalauncher.singletons.ChromaApplication;
import com.rcmz.chromalauncher.R;
import com.rcmz.chromalauncher.model.Utils;
import com.rcmz.chromalauncher.adapters.AppEntryAdapter;
import com.rcmz.chromalauncher.fragments.AppEntryBottomSheetDialogFragment;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LauncherActivity extends AppCompatActivity {
    private BroadcastReceiver packageChangeBroadcastReceiver;
    private SharedPreferences prefs;
    private Resources res;
    private SharedPreferences.OnSharedPreferenceChangeListener sharedPreferenceChangeListener;
    private List<AppEntry> appEntries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.launcher_activity);

        // update the app list if an app in installed / changed / removed
        packageChangeBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ChromaApplication.getAppRepository().refresh();
            }
        };

        IntentFilter packageChangeIntentFilter = new IntentFilter();
        packageChangeIntentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
        packageChangeIntentFilter.addAction(Intent.ACTION_PACKAGE_CHANGED);
        packageChangeIntentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        packageChangeIntentFilter.addDataScheme("package");
        registerReceiver(packageChangeBroadcastReceiver, packageChangeIntentFilter);

        // update the grid if a preference is changed
        sharedPreferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                updateGrid();
            }
        };

        prefs = PreferenceManager.getDefaultSharedPreferences(ChromaApplication.getContext());
        prefs.registerOnSharedPreferenceChangeListener(sharedPreferenceChangeListener);
        res = getResources();

        ChromaApplication.getAppRepository().getVisible().observe(this, new Observer<List<AppEntry>>() {
            @Override
            public void onChanged(List<AppEntry> changedAppEntries) {
                appEntries = changedAppEntries;
                updateGrid();
            }
        });
    }

    private void updateGrid() {
        final GridView gridView = findViewById(R.id.launcher_activity_grid_view);

        // set background/bars color
        @ColorInt int backgroundColor;

        if (prefs.getString("background_mode", res.getString(R.string.pref_def_background_mode)).equals("wallpaper")) {
            float wallpaperOpacity = ((float) prefs.getInt("wallpaper_opacity", res.getInteger(R.integer.pref_def_wallpaper_opacity))) / 100f;
            int alpha = (int) ((1f - wallpaperOpacity) * 255f);
            backgroundColor = Color.argb(alpha, 0, 0, 0);
        } else {
            backgroundColor = Color.BLACK;
        }

        gridView.setBackgroundColor(backgroundColor);
        getWindow().setStatusBarColor(backgroundColor);
        getWindow().setNavigationBarColor(backgroundColor);

        // sort the entries base on their hue
        final boolean colorlessAppsFirst = prefs.getBoolean("colorless_apps_first", res.getBoolean(R.bool.pref_def_colorless_apps_first));
        final boolean inverseHueOrder = prefs.getBoolean("inverse_hue_order", res.getBoolean(R.bool.pref_def_inverse_hue_order));
        final float hueOffset = (float) prefs.getInt("hue_offset", res.getInteger(R.integer.pref_def_hue_offset));

        Collections.sort(appEntries, new Comparator<AppEntry>() {
            @Override
            public int compare(AppEntry a1, AppEntry a2) {
                @ColorInt int c1 = a1.color;
                @ColorInt int c2 = a2.color;

                // sort colorless apps
                float s1 = Utils.saturation(c1);
                float s2 = Utils.saturation(c2);
                if (s1 == 0f && s2 == 0f) return a1.packageName.compareTo(a2.packageName);
                if (s1 == 0f) return colorlessAppsFirst ? -1 : +1;
                if (s2 == 0f) return colorlessAppsFirst ? +1 : -1;

                // sort colorful apps
                float h1 = (Utils.hue(c1) + hueOffset) % 360f;
                float h2 = (Utils.hue(c2) + hueOffset) % 360f;
                if (h1 == h2) return a1.packageName.compareTo(a2.packageName);
                return Float.compare(h1, h2) * (inverseHueOrder ? -1 : 1);
            }
        });

        // set number of columns
        if (prefs.getBoolean("auto_columns", res.getBoolean(R.bool.pref_def_auto_columns))) {
            gridView.post(new Runnable() {
                @Override
                public void run() {
                    int columns = 1;
                    float bestRatio = 0;

                    for (int c = 1; c <= appEntries.size(); c++) {
                        int rows = appEntries.size() / c + (appEntries.size() % c > 0 ? 1 : 0);
                        int cellWidth = gridView.getWidth() / c;
                        int cellHeight = gridView.getHeight() / rows;
                        float ratio = (float) cellWidth / (float) cellHeight;

                        if (Math.abs(ratio - 1) < Math.abs(bestRatio - 1)) {
                            bestRatio = ratio;
                            columns = c;
                        }
                    }

                    gridView.setNumColumns(columns);
                }
            });
        } else {
            gridView.setNumColumns(prefs.getInt("number_of_columns", res.getInteger(R.integer.pref_def_number_of_columns)));
        }

        gridView.setAdapter(new AppEntryAdapter(appEntries));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AppEntry appEntry = (AppEntry) gridView.getAdapter().getItem(position);
                Intent appIntent = Intent.makeMainActivity(new ComponentName(appEntry.packageName, appEntry.className));
                appIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appIntent, ChromaApplication.slideUpBundle);
            }
        });

        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                AppEntry appEntry = (AppEntry) gridView.getAdapter().getItem(position);
                AppEntryBottomSheetDialogFragment fragment = new AppEntryBottomSheetDialogFragment(appEntry);
                fragment.show(getSupportFragmentManager(), fragment.getTag());
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(packageChangeBroadcastReceiver);
        prefs.unregisterOnSharedPreferenceChangeListener(sharedPreferenceChangeListener);
        super.onDestroy();
    }
}
