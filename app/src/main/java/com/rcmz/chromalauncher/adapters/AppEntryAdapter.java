package com.rcmz.chromalauncher.adapters;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import androidx.core.graphics.ColorUtils;
import androidx.preference.PreferenceManager;

import com.rcmz.chromalauncher.R;
import com.rcmz.chromalauncher.drawables.NineGradientDrawable;
import com.rcmz.chromalauncher.singletons.ChromaApplication;
import com.rcmz.chromalauncher.model.AppEntry;

import java.util.List;

public class AppEntryAdapter extends BaseAdapter {
    private List<AppEntry> appEntries;
    private SharedPreferences prefs;
    private Resources res;

    public AppEntryAdapter(List<AppEntry> appEntries) {
        this.appEntries = appEntries;
        prefs = PreferenceManager.getDefaultSharedPreferences(ChromaApplication.getContext());
        res = ChromaApplication.getContext().getResources();
    }

    @Override
    public int getCount() {
        return appEntries.size();
    }

    @Override
    public AppEntry getItem(int position) {
        return appEntries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public ImageView getView(int position, View convertView, ViewGroup parent) {
        AppEntry appEntry = getItem(position);
        ImageView imageView = new ImageView(ChromaApplication.getContext());

        imageView.setImageDrawable(appEntry.icon);

        if (prefs.getString("background_mode", res.getString(R.string.pref_def_background_mode)).equals("icon_color")) {
            imageView.setBackgroundColor(appEntry.color);
        }

        GridView gridView;
        gridView = (GridView) parent;
        int columns = gridView.getNumColumns();
        int rows = getCount() / columns + (getCount() % columns > 0 ? 1 : 0);
        int c = position % columns;
        int r = position / columns;

        float exactRowHeight = (float) gridView.getHeight() / rows;
        int rowTop = Math.round(r * exactRowHeight);
        int rowTopNext = Math.round((r + 1) * exactRowHeight);
        int rowHeight = rowTopNext - rowTop;

        float exactColumnWidth = (float) gridView.getWidth() / columns;
        int columnLeft = Math.round(c * exactColumnWidth);
        int columnLeftNext = Math.round((c + 1) * exactColumnWidth);
        int columnWidth = columnLeftNext - columnLeft;

        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, rowHeight));

        Matrix matrix = new Matrix();
        matrix.setRectToRect(
                new RectF(0, 0, imageView.getDrawable().getIntrinsicWidth(), imageView.getDrawable().getIntrinsicHeight()),
                new RectF(0, 0, exactColumnWidth, exactRowHeight),
                Matrix.ScaleToFit.CENTER);
        float scale = ((float) prefs.getInt("icon_size", res.getInteger(R.integer.pref_def_icon_size))) / 100f;
        matrix.postScale(scale, scale, exactColumnWidth / 2, exactRowHeight / 2);
        imageView.setImageMatrix(matrix);
        imageView.setScaleType(ImageView.ScaleType.MATRIX);

        return imageView;
    }
}
