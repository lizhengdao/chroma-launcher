package com.rcmz.chromalauncher.drawables;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.rcmz.chromalauncher.model.BetterPalette;

public class RainbowDrawable extends Drawable {
    private final BetterPalette palette;

    public RainbowDrawable(BetterPalette palette) {
        this.palette = palette;
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        Rect bounds = copyBounds();
        int padding = 0;
        bounds.inset(padding, padding);

        if (palette == null || palette.getColors().isEmpty()) {
            return;
        }

        int populationTotal = 0;

        for (@ColorInt int color : palette.getColors()) {
            populationTotal += palette.getPopulation(color);
        }

        if (populationTotal == 0) {
            return;
        }

        int populationSum = 0;

        for (@ColorInt int color : palette.getColors()) {
            ColorDrawable sliceDrawable = new ColorDrawable(color);

            int left = bounds.left;
            int right = bounds.right;
            int top = bounds.top + bounds.height() * populationSum / populationTotal;
            populationSum += palette.getPopulation(color);
            int bottom = bounds.top + bounds.height() * populationSum / populationTotal;

            Rect sliceBounds = new Rect(left, top, right, bottom);
            sliceDrawable.setBounds(sliceBounds);
            sliceDrawable.draw(canvas);
        }
    }

    @Override
    public void setAlpha(int i) {
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
    }

    @Override
    public int getOpacity() {
        return PixelFormat.UNKNOWN;
    }
}
